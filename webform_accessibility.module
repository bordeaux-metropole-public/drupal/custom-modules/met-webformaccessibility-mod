<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\webform\Entity\Webform;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformThirdPartySettingsManagerInterface;

/**
 * Implements hook_menu_local_tasks_alter().
 * Add "Webform" tabs to node bundle configured (Default 'webform')
 */
function webform_accessibility_menu_local_tasks_alter(&$data, $route_name)
{
  $node = \Drupal::routeMatch()->getParameter('node');
  //If this is not a node, exit.
  if(!isset($node)) return;
  /** @var WebformThirdPartySettingsManagerInterface $third_party_settings_manager */
  $third_party_settings_manager = \Drupal::service('webform.third_party_settings_manager');
  $bundles = $third_party_settings_manager->getThirdPartySetting('webform_accessibility','bundles');

  //If the node bundle is not one of those configured, exit.
  if(array_search($node->bundle(), explode(',', $bundles)) === false)
    return;

  $user = \Drupal::currentUser();
  if($node->hasField('webform')) {
    $webform_id = $node->get('webform')->getValue()[0]['target_id'] ?? NULL;
    if (isset($webform_id) && !empty($webform_id)) {
      $url = Url::fromRoute('entity.webform.edit_form', ['webform' => $webform_id]);

      /** @var WebformInterface $webform_entity */
      $webform_entity = Webform::load($webform_id);
      //If tabs redirect to edit form page and user cannot edit it, so exit.
      if ($webform_entity == NULL || !$webform_entity->access('update'))
        return;
    } else {
      $url = Url::fromRoute('entity.webform.add_form');
      //If tabs redirect to add form page and user cannot create one or cannot edit the current node, so exit
      if (!$user->hasPermission('create webform') || !$node->access('update'))
        return;
    }
  }
  // Add a tab linking to webform.edit page or webform.add page.
  $data['tabs'][0]['node.add_page'] = array(
    '#theme' => 'menu_local_task',
    '#link' => array(
      'title' => t('Webform'),
      'url' => $url,
      'localized_options' => array(
        'attributes' => array(
          'title' => t('Webform'),
        ),
      ),
    ),
  );
}

/**
 * Allow configuration.
 * implement hook_webform_admin_third_party_settings_form_alter()
 * @param $form
 * @param $form_state
 */
function webform_accessibility_webform_admin_third_party_settings_form_alter(&$form, FormStateInterface $form_state)
{
  //webform_admin_config_forms_form
  $form['third_party_settings']['webform_accessibility']= [
    '#type' => 'details',
    '#title' => t('Webform Accessibility'),
    '#description' => t('Add a tabs "Webform" on node to manage the form link to the node.'),
  ];
  /** @var WebformThirdPartySettingsManagerInterface $third_party_settings_manager */
  $third_party_settings_manager = \Drupal::service('webform.third_party_settings_manager');
  $form['third_party_settings']['webform_accessibility']['bundles'] = [
    '#type' => 'textfield',
    '#title' => t('Webform Accessibility Bundle'),
    '#description' => t('Add content type ID that need to have the "Webform" tabs. Multiple value must be separated by a ",".'),
    '#default_value' => $third_party_settings_manager->getThirdPartySetting('webform_accessibility','bundles'),
  ];
}
