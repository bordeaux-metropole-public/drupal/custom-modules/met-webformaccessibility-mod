# Webform Accesibility Module

MET-WEBFORMACCESSIBILITY-MOD

## Description

This module adds more accessibility to webform from node by adding a new tabs "Webform" which appears on some specific content type. This tabs redirect to the build section of the webform attach to the node. If no webform is attach, the tabs redirect to the "Add webform" page.

By default the tab appears only on "webform" node bundle.This can be modify in `/admin/structure/webform/config` at the `Third Party Settings` field under `WEBFORM ACCESSIBILITY`.
Multiple values must be separated by a comma. Exemple :
* webform
* webform,article

## Installation

1. Place the module folder in your modules folder
2. Make sure you have the webform module and webform node enabled
3. Activate the module via admin/modules
